const asyncHandler = require("express-async-handler");
const { Task } = require("../models");

const GetAllTask = asyncHandler(async (req, res) => {
  const tasks = await Task.findAll({
    where: {
      user_id: req.userId,
    },
  });

  res.status(200).send(tasks);
});

const CreateNewTask = asyncHandler(async (req, res) => {
  const newTask = await Task.create({
    user_id: req.userId,
    task_title: req.body.title,
    task_content: req.body.content,
    task_status: req.body.status,
  });

  res.status(201).send(newTask);
});

const UpdateTask = asyncHandler(async (req, res) => {});

module.exports = { GetAllTask, CreateNewTask, UpdateTask };
