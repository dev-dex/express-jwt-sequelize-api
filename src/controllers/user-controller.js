const { User } = require("../models");
const asyncHandler = require("express-async-handler");
const bcrypt = require("bcrypt");
const { GenerateAccessToken, GenerateRefreshToken } = require("../utilities/generate-token");
const StoreCookieToken = require("../utilities/store-cookie");

const CreateNewUser = asyncHandler(async (req, res) => {
  // * desc:    CREATE NEW USERS
  // * routes:  POST /api/users/create
  // * access:  PUBLIC

  const user = await User.findOne({
    where: {
      user_email: req.body.email,
    },
  });

  if (user) {
    res.status(409);
    throw new Error("Email address is already taken.");
  }

  const hashPass = await bcrypt.hash(req.body.password, 10);

  const createdUser = await User.create({
    user_name: req.body.name,
    user_email: req.body.email,
    user_password: hashPass,
  });

  res.status(201).send({
    message: "New user created successfully.",
    user: createdUser,
  });
});

const AuthenticateUser = asyncHandler(async (req, res) => {
  // * desc:    Authenticate User to Access Private Route
  // * routes:  POST /api/users/authenticate
  // * access:  PUBLIC

  const user = await User.findOne({
    where: {
      user_email: req.body.email,
    },
  });

  if (!user) {
    res.status(401);
    throw new Error("Email and Password does not match");
  }

  const passwordMatch = await bcrypt.compare(req.body.password, user.user_password);

  if (!passwordMatch) {
    res.status(401);
    throw new Error("Email and Password does not match");
  }

  const accessToken = GenerateAccessToken(user.user_id);
  const refreshToken = GenerateRefreshToken(user.user_id);

  StoreCookieToken(res, "refreshToken", refreshToken);

  res.status(200).send(accessToken);
});

const GetAuthorizeUser = asyncHandler(async (req, res) => {
  const user = await User.findByPk(req.userId, { include: ["Task"] });
  res.send(user);
});

const DeAuthenticateUser = asyncHandler(async (req, res) => {
  res.clearCookie("refreshToken");
  res.sendStatus(200);
});

module.exports = { CreateNewUser, AuthenticateUser, GetAuthorizeUser, DeAuthenticateUser };
