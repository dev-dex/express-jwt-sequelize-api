require("dotenv").config();

const app = require("./app");
const connection = require("./database/connection");

const SERVER_PORT = process.env.SERVER_PORT || 5000;

const StartServer = async () => {
  try {
    await connection.sync();
    console.log(`Connection has been established successfully.`);
    app.listen(SERVER_PORT, () => {
      console.log(`Listening: http://127.0.0.1:${SERVER_PORT}`);
    });
  } catch (error) {
    console.error(`Unable to connect to the database: ${error}`);
  }
};

StartServer();
