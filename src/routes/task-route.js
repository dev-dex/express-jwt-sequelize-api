const express = require("express");
const { GetAllTask, CreateNewTask } = require("../controllers/task-controller");
const { ValidateTask } = require("../validators/task-validator");

const router = express.Router();

router.get("/", GetAllTask);

router.post("/", ValidateTask, CreateNewTask);

module.exports = router;
