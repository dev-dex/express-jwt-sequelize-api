const express = require("express");
const {
  CreateNewUser,
  AuthenticateUser,
  GetAuthorizeUser,
  DeAuthenticateUser,
} = require("../controllers/user-controller");
const { ValidateNewUser, ValidateUser } = require("../validators/user-validator");
const RequiredAuthentication = require("../middlewares/authorization");

const router = express.Router();

router.get("/", RequiredAuthentication, GetAuthorizeUser);

router.post("/create", ValidateNewUser, CreateNewUser);

router.post("/authenticate", ValidateUser, AuthenticateUser);

router.post("/deauthenticate", DeAuthenticateUser);

module.exports = router;
