const connection = require("../database/connection");
const { DataTypes, UUIDV4 } = require("sequelize");

const Task = connection.define(
  "Task",
  {
    user_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    task_id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: UUIDV4,
    },
    task_title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    task_content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    task_status: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    createdAt: "created_at",
    updatedAt: "updated_at",
    freezeTableName: true,
    modelName: "Task",
  }
);

Task.prototype.toJSON = function () {
  return {
    ...this.get(),
    user_id: undefined,
  };
};
module.exports = Task;
