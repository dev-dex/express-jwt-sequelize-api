const User = require("./user");
const Task = require("./task");

User.hasMany(Task, { foreignKey: "user_id", as: "Task" });
Task.belongsTo(User, { foreignKey: "user_id", as: "User" });

module.exports = { User, Task };
