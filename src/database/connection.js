const config = require("./config");
const { Sequelize } = require("sequelize");

const connection = new Sequelize(config[process.env.NODE_ENV]);

module.exports = connection;
