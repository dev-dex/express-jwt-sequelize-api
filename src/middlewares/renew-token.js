const asyncHandler = require("express-async-handler");
const jwt = require("jsonwebtoken");
const { GenerateAccessToken } = require("../utilities/generate-token");

const RenewAccessToken = asyncHandler(async (req, res) => {
  const refreshToken = req.cookies.refreshToken;

  if (!refreshToken) {
    res.status(401);
    throw new Error("Missing refresh token in the cookie.");
  }

  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, decoded) => {
    if (err) {
      res.status(403);
      throw new Error("The provided token is invalid.");
    }

    const newAccessToken = GenerateAccessToken(decoded.userId);

    res.status(200).send(newAccessToken);
  });
});

module.exports = RenewAccessToken;
