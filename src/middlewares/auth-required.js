const asyncHandler = require("express-async-handler");
const jwt = require("jsonwebtoken");

const RequiredAuthentication = asyncHandler(async (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const accessToken = authHeader && authHeader.split(" ")[1];

  if (!accessToken) {
    res.status(401);
    throw new Error("Authorization header with token is required.");
  }

  jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
    if (err) {
      res.status(403);
      throw new Error("The provided token is invalid.");
    }

    req.userId = decoded.userId;
    next();
  });
});

module.exports = RequiredAuthentication;
