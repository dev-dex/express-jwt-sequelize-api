const jwt = require("jsonwebtoken");

const GenerateAccessToken = (id) => {
  return jwt.sign({ userId: id }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: process.env.ACCESS_TOKEN_EXPIRATION,
  });
};

const GenerateRefreshToken = (id) => {
  return jwt.sign({ userId: id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: process.env.REFRESH_TOKEN_EXPIRATION,
  });
};

module.exports = { GenerateAccessToken, GenerateRefreshToken };
