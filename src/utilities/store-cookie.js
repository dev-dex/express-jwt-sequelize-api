const StoreCookieToken = (res, name, value) => {
  return res.cookie(name, value, {
    httpOnly: true,
    expires: new Date(
      Date.now() + process.env.COOKIE_EXPIRATION_DAY * 24 * 60 * 60 * 1000
    ),
  });
};

module.exports = StoreCookieToken;
