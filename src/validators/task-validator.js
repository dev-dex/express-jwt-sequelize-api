const { body, validationResult } = require("express-validator");

const ValidateTask = [
  body("title")
    .notEmpty()
    .withMessage("Title is required")
    .isLength({ min: 3, max: 30 })
    .withMessage("Title must be between 3 and 30 characters"),
  body("content")
    .notEmpty()
    .withMessage("Content is required")
    .isLength({ min: 3, max: 30 })
    .withMessage("Content must be between 3 and more characters"),
  body("status")
    .notEmpty()
    .withMessage("Status is required")
    .isLength({ min: 3, max: 30 })
    .withMessage("Status must be between 3 and 15 characters"),
  (req, res, next) => {
    const allowedFields = ["title", "content", "status"];
    const receiveFields = Object.keys(req.body);

    const extraFields = receiveFields.filter((field) => !allowedFields.includes(field));

    if (extraFields.length > 0) {
      res.status(400);
      throw new Error(`Unexpected field '${extraFields[0]}'`);
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      // Get the first error message
      const firstError = errors.array()[0].msg;
      res.status(400);
      throw new Error(firstError);
    }
    next();
  },
];

module.exports = { ValidateTask };
