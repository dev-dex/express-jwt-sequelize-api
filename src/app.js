const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const ErrorHandler = require("./middlewares/error-handler");
const NotFound = require("./middlewares/not-found");
const RenewAccessToken = require("./middlewares/renew-token");
const UserRoutes = require("./routes/user-route");
const TaskRoutes = require("./routes/task-route");
const RequiredAuthentication = require("./middlewares/authorization");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan("dev"));
app.use(cors());
app.use(cookieParser());

app.get("/", (req, res) => {
  res.status(200).send({
    message: "Hello World! 👋🌏✨",
  });
});

app.use("/api/users", UserRoutes);
app.use("/auth/token/renew", RenewAccessToken);
app.use("/api/protected/tasks", RequiredAuthentication, TaskRoutes);

app.use(NotFound);
app.use(ErrorHandler);

module.exports = app;
